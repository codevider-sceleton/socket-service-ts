FROM node:alpine

RUN mkdir -p /srv/socket

WORKDIR /srv/socket

COPY package.json package-lock.json ./

RUN npm install

COPY . ./

EXPOSE 3002

CMD npm run dev