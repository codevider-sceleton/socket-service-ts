import * as dotenv from 'dotenv';
import * as path from 'path';
import {
  getOsEnv,
  getOsPaths,
  normalizePort,
  toBool,
  getOsEnvOptional,
} from '../src/Util/EnvUtils';
import * as pkg from '../package.json';

const env = process.env;
const devMode = !env.NODE_ENV || env.NODE_ENV === 'dev';

const dotenvConfig = {
  path: path.join(process.cwd(), `.env${devMode ? '.dev' : ''}`),
};

dotenv.config(dotenvConfig);

export const config = {
  node: process.env.NODE_ENV || 'dev',
  isProduction: process.env.NODE_ENV === 'prod',
  isTest: process.env.NODE_ENV === 'test',
  isDevelopment: process.env.NODE_ENV === 'dev',
  projectRoot: __dirname + '/../',
  app: {
    name: getOsEnv('APP_NAME'),
    version: (pkg as any).version,
    description: (pkg as any).description,
    host: getOsEnv('APP_HOST'),
    schema: getOsEnv('APP_SCHEMA'),
    port: normalizePort(process.env.PORT || getOsEnv('APP_PORT')),
    routePrefix: getOsEnv('APP_ROUTE_PREFIX'),
    banner: toBool(getOsEnv('APP_BANNER')),
    dirs: {
      controllers: getOsPaths('CONTROLLERS'),
      middlewares: getOsPaths('MIDDLEWARES'),
      interceptors: getOsPaths('INTERCEPTORS'),
    },
  },
  log: {
    level: getOsEnv('LOG_LEVEL'),
    json: toBool(getOsEnvOptional('LOG_JSON') as string),
    output: getOsEnv('LOG_OUTPUT'),
  },
};
