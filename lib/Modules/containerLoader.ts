import { useContainer as classValidatorUseContainer } from 'class-validator';
import { MicroframeworkLoader } from 'microframework';
import { Container } from 'typedi';

export const containerLoader: MicroframeworkLoader = () => {
  /**
   * Setup socket-controllers to use typedi container.
   */
  classValidatorUseContainer(Container);
};
