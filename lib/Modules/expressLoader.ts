import { Request, Response, application } from 'express';
import express from 'express';
import * as path from 'path';
import { MicroframeworkLoader, MicroframeworkSettings } from 'microframework';
import { useExpressServer } from 'routing-controllers';

import { config } from '../../config/config';

export const expressLoader: MicroframeworkLoader = (
  settings?: MicroframeworkSettings
) => {
  if (settings) {

    /**
     * We create a new express server instance.
     * We could have also use useExpressServer here to attach controllers to an existing express instance.
     */
    // @ts-ignore
    const expressApp = express();

    useExpressServer(expressApp, {
      cors: {
        credentials: true,
        origin: true,
      },
      classTransformer: true,
      routePrefix: config.app.routePrefix,
      defaultErrorHandler: true,
      /**
       * We can add options about how routing-controllers should configure itself.
       * Here we specify what controllers should be registered in our express server.
       */
      controllers: config.app.dirs.controllers,
      middlewares: config.app.dirs.middlewares,
      interceptors: config.app.dirs.interceptors,
      validation: false,
    });
    expressApp.use(express.static(path.join(__dirname + '../../../src/public')));
    // Run application to listen on given port
    if (!config.isTest) {
      const server = expressApp.listen(config.app.port);
      settings.setData('express_server', server);
    }
    // application entrypoint
    expressApp.get(config.app.routePrefix, (req: Request, res: Response) => {
      return res.sendFile(path.join(__dirname + '/../../../src/public/index.html'));
    });

    // Here we can set the data for other loaders
    settings.setData('express_app', expressApp);
  }
};
