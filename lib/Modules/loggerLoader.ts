import { MicroframeworkLoader, MicroframeworkSettings } from 'microframework';
import { configure, format, transports } from 'winston';

import { config } from '../../config/config';

export const loggerLoader: MicroframeworkLoader = (settings?: MicroframeworkSettings) => {
  configure({
    transports: [
      new transports.Console({
        level: config.log.level,
        handleExceptions: true,
        format: config.node !== 'dev'
          ? format.combine(
            format.json()
          )
          : format.combine(
            format.colorize(),
            format.simple()
          ),
      }),
    ],
  });
};
