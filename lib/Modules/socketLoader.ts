import { MicroframeworkLoader, MicroframeworkSettings } from 'microframework';
import { useSocketServer } from 'socket-controllers';
import { config } from '../../config/config';

export const socketLoader: MicroframeworkLoader = (
  settings?: MicroframeworkSettings
) => {
  if (settings) {
    const expressApp = settings.getData('express_server');
    const io = require('socket.io')(expressApp);
    /**
     * We create a new socket server instance.
     * We could have also use useSocketServer here to attach socket to an existing express instance.
     */
    const socketApp = useSocketServer(io, {
      useClassTransformer: true,
      controllers: config.app.dirs.controllers,
      middlewares: config.app.dirs.middlewares,
    });
    // Here we can set the data for other loaders
    settings.setData('socket_app', socketApp);
  }
};
