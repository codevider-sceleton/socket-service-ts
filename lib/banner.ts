import { config } from '../config/config';
import { Logger } from './Logger';
import chalk from 'chalk';

export function banner(log: Logger): void {
  const route = () =>
    `${config.app.schema}://${config.app.host}:${config.app.port}`;
  if (config.app.banner) {
    log.info(``);
    log.info('App is running');
    log.info(`To shut it down, press <CTRL> + C at any time.`);
    log.info(``);
    log.info('-------------------------------------------------------');
    log.info(`Environment  : ${config.node}`);
    log.info(`Version      : ${config.app.version}`);
    log.info(``);
    log.info(chalk.green(`Socket Info     : ${route()}`));
    log.info('-------------------------------------------------------');
    log.info('');
  } else {
    log.info(`Application is up and running on ${chalk.green(route())}.`);
  }
}
