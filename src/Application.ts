import 'reflect-metadata';

import { bootstrapMicroframework } from 'microframework';
import { Logger } from '../lib/Logger/Logger';
import { containerLoader, loggerLoader, socketLoader, expressLoader } from '../lib/Modules';
import { banner } from '../lib/banner';

const log = new Logger();

bootstrapMicroframework({
  config: {
    showBootstrapTime: true,
    bootstrapTimeout: 5,
  },
  loaders: [loggerLoader, containerLoader, expressLoader, socketLoader],
})
  .then(() => banner(log))
  .catch((error) => log.error(`Application crashed: ${error}`));
