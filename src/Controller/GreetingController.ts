import {
  SocketController,
  OnConnect,
  ConnectedSocket,
  OnDisconnect,
  OnMessage,
  MessageBody,
  NspParams,
  SocketIO
} from 'socket-controllers';
import { Logger } from '../../lib/Logger/Logger';
const log = new Logger();
@SocketController('/hello')
export class GreetingController {
  @OnConnect()
  public connection(@ConnectedSocket() socket: any): void {
    log.info('client connected');
    socket.username = 'Anonymous';
    socket.channel = 'message';
    socket.join('hello');
  }

  @OnDisconnect()
  public disconnect(@ConnectedSocket() socket: any): void {
    log.info('client disconnected');
  }

  @OnMessage('save')
  public async save(
    @ConnectedSocket() socket: any,
    @MessageBody() message: any,
    @NspParams() params: any[]
  ): Promise<void> {
    log.info('received message:', message);
    log.info('namespace params:', params);
    log.info('setting id to the message and sending it back to the client');
    message.id = 1;
    socket.emit('message_saved', { message: message.message, username: socket.username });
    socket.to('hello').emit('message_saved', { message: message.message, username: socket.username });
  }
  @OnMessage('change_username')
  public async changeUsername(
    @ConnectedSocket() socket: any,
    @MessageBody() message: any,
    @NspParams() params: any[],
    @SocketIO() io: any
  ): Promise<void> {
    log.info('received username:', message.username);
    socket.username = message.username;

  }

  @OnMessage('typing')
  public async typing(
    @ConnectedSocket() socket: any
  ): Promise<void> {
    socket.to('hello').emit('typing', { username: socket.username });
  }
}
