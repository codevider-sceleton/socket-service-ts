$(function () {
    //make connection
    var socket = io.connect('/hello')

    //buttons and inputs
    var message = $("#message")
    var username = $("#username")
    var send_message = $("#send_message")
    var send_username = $("#send_username")
    var chatroom = $("#chatcontent")
    var feedback = $("#feedback")

    //Emit message
    send_message.click(function () {
        socket.emit('save', { message: message.val() })
    })

    //Listen on message_saved
    socket.on("message_saved", (data) => {
        feedback.html('');
        message.val('');
        chatroom.append("<p class='message'>" + data.username + ": " + data.message + "</p>")
    })

    //Emit a username
    send_username.click(function () {
        socket.emit('change_username', { username: username.val() })
    })

    //Emit typing
    message.bind("keypress", () => {
        socket.emit('typing')
    })

    //Listen on typing
    socket.on('typing', (data) => {
        console.log(data);
        feedback.html("<p><i>" + data.username + " is typing a message..." + "</i></p>")
    })
});